package com.homework4;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void checkGetCount(CountMap<Integer> map){
        System.out.println("=========== Количество элементов ===========");
        System.out.println(map.getCount(5)); // 2
        System.out.println(map.getCount(6)); // 1
        System.out.println(map.getCount(10)); // 3
    }

    public static void checkRemove(CountMap<Integer> map){
        System.out.println("== Количество элементов после удаления 10 ==");
        System.out.println(map.remove(10));
        System.out.println(map.remove(10));
        System.out.println(map.remove(10));
        System.out.println(map.remove(10));
    }

    public static void checkSize(CountMap<Integer> map){
        System.out.println("========= Количество всех элементов =========");
        System.out.println(map.size());
    }

    public static void checkAddAll(CountMap<Integer> map1, CountMap<Integer> map2){
        System.out.println("==== Добавление элементов из map2 в map1 ====");
        System.out.println(map1.size());
        map1.addAll(map2);
        System.out.println(map1.size());
    }

    public static void main(String[] args) {
        CountMap<Integer> map1 = new CountMapIml<>();
        map1.add(10);
        map1.add(10);
        map1.add(5);
        map1.add(6);
        map1.add(5);
        map1.add(10);
        checkGetCount(map1);
        checkRemove(map1);
        checkSize(map1);

        CountMap<Integer> map2 = new CountMapIml<>();
        map2.add(10);
        map2.add(10);
        map2.add(5);
        map2.add(6);
        map2.add(5);
        map2.add(10);

        checkAddAll(map1, map2);

        System.out.println("==== Сделать map  из CountMapIml ====");
        Map map3 = map1.toMap();
        System.out.println(map3);

        Map map4 = new HashMap<>();
        map4.put(3, 2);
        map4.put(5, 1);
        System.out.println("==== Заполнить map из CountMapIml ====");
        map1.toMap(map4);

    }
}
