package com.homework4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CountMapIml<E> implements CountMap<E> {

    private List<E> keys = new ArrayList<>();
    private List<Integer> values = new ArrayList<>();

    public List<E> getKeys() {
        return keys;
    }

    public List<Integer> getValues() {
        return values;
    }

    @Override
    public void add(E object) {   // добавляет элемент в этот контейнер.
        if (keys.contains(object)) {
            values.set(keys.indexOf(object), values.get(keys.indexOf(object)) + 1);
        } else {
            keys.add(object);
            values.add(1);
        }
    }

    @Override
    public int getCount(E object) { //Возвращает количество добавлений данного элемента
        if (keys.contains(object)) {
            return values.get(keys.indexOf(object));
        }
        return 0;
    }

    @Override
    public int remove(E o) { //Удаляет элемент, и контейнер и возвращает количество его добавлений(до удаления)
        if (keys.contains(o)) {
            int index = keys.indexOf(o);
            int count = values.get(index);
            if (count > 1) {
                values.set(index, values.get(index) - 1);
            } else {
                values.remove(index);
                keys.remove(index);
            }
            return count;
        }
        return 0;
    }

    @Override
    public int size() { //количество разных элементов
        Integer count = 0;
        for (Integer value : values) {
            count += value;
        }
        return count;
    }

    @Override
    public void addAll(CountMap<E> source) { //Добавить все элементы из source в текущий контейнер, при совпадении ключей - суммировать значения
        List<E> sourceKeys = source.getKeys();
        List<Integer> sourceValues = source.getValues();
        for (E object : sourceKeys) {
            int indexSource = sourceKeys.indexOf(object);
            if (keys.contains(object)) {
                values.set(keys.indexOf(object), values.get(keys.indexOf(object)) + sourceValues.get(indexSource));
            } else {
                keys.add(object);
                values.add(sourceValues.get(indexSource));
            }
        }
    }

    @Override
    public Map<E, Integer> toMap() { //Вернуть java.util.Map. ключ - добавленный элемент, значение - количество его добавлений
        Map<E, Integer> map = new HashMap();
        for (E key : keys) {
            map.put(key, values.get(keys.indexOf(key)));
        }
        return map;
    }

    @Override
    public void toMap(Map<E, Integer> destination) { //Тот же самый контракт как и toMap(), только всю информацию записать в destination
        for (E key : keys) {
            Integer value = values.get(keys.indexOf(key));
            if (destination.containsKey(key)) {
                destination.put(key, destination.get(key) + value);
            } else {
                destination.put(key, value);
            }
        }
    }
}
